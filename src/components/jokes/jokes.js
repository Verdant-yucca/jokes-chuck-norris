const selectors = {
  buttonLike: ".js-button-like",
  buttonAdd: ".js-button-add-jokes",
  spinnerAddJokes: ".js-spinner-add-jokes",
  containerJokes: ".js-container-jokes",
  templateJokes: ".js-template-jokes",
  templateJokesDescription: ".js-jokes-card-description",
};

const classes = {
  spinnerVisible: 'spinner_visible',
  buttonLikeActive: 'jokes-card__button_active',
}

const buttonsAdd = document.querySelector(selectors.buttonAdd);
const containerJokes = document.querySelector(selectors.containerJokes);
const spinnerAddJokes = document.querySelector(selectors.spinnerAddJokes);
const url = 'https://api.chucknorris.io/jokes/random';
let pressedButtonAddJokes = false;
const arrLikedJokesToLocalStorage = JSON.parse(localStorage.getItem('arrLikedJokes'));
let arrLikedJokes = arrLikedJokesToLocalStorage ? arrLikedJokesToLocalStorage : [];
let interval = '';

const handlerAddJokes = () => {
  if (!pressedButtonAddJokes) {
    pressedButtonAddJokes = true;
    spinnerAddJokes.classList.add(classes.spinnerVisible);
    interval = setInterval(() => {
      fetch(url)
        .then(response => {
          if (response.ok) return response.json();
          else return Promise.reject(response.status);
        })
        .then(json => {
          const templateJokes = document.querySelector(selectors.templateJokes).content.cloneNode(true);
          templateJokes.querySelector(selectors.templateJokesDescription).textContent = json.value;
          containerJokes.append(templateJokes);
        })
        .catch(err => {
          if (err > 399 && err < 500) throw new Error('клиентская ошибка');
          else if (err > 499 && err < 600) throw new Error('серверная ошибка');
          else throw new Error(err);
        });
    }, 3000)
  } else {
    clearInterval(interval);
    pressedButtonAddJokes = false;
    spinnerAddJokes.classList.remove(classes.spinnerVisible);
  }
}

const checkCountItemToArrayLessThan = (arr, count) => arr.length < count;

const addItemToArray = (arr, item) => {
  if (!checkCountItemToArrayLessThan(arr, 10)) arr.splice(0, 1);
  arr.push(item);
  return arr;
}

const removeItemFromArray = (arr, item) => {
  arr.splice(arr.indexOf(item), 1)
  return arr;
}

const handlerClickLike = (e) => {
  const target = e.target;
  if (target.tagName === 'BUTTON' || target.tagName === 'svg' || target.tagName === 'path') {
    const button = target.closest(selectors.buttonLike);
    const quote = button.nextElementSibling.textContent;
    if (button.classList.contains(classes.buttonLikeActive)) {
      button.classList.remove(classes.buttonLikeActive);
      removeItemFromArray(arrLikedJokes, quote);
    } else {
      button.classList.add(classes.buttonLikeActive);
      addItemToArray(arrLikedJokes, quote);
    }
    localStorage.setItem("arrLikedJokes", JSON.stringify(arrLikedJokes));
  }
}

buttonsAdd.addEventListener("click", handlerAddJokes);
containerJokes.addEventListener("click", handlerClickLike);
