const selectors = {
  buttonDelete: ".js-button-delete-favourites",
  buttonDeleteAll: ".js-button-delete-all-favourites-jokes",
  containerJokes: ".js-container-favourites-jokes",
  jokesFavouritesDescription: ".js-jokes-favourites-description",
  templateFavouritesJokes: ".js-template-favourites-jokes",
};

const classes = {
  title: 'title'
}

const buttonDeleteAll = document.querySelector(selectors.buttonDeleteAll);
const containerJokes = document.querySelector(selectors.containerJokes);
const arrLikedJokesToLocalStorage = JSON.parse(localStorage.getItem('arrLikedJokes'));
let arrLikedJokes = arrLikedJokesToLocalStorage ? arrLikedJokesToLocalStorage : [];

const printJokeToPage = (arr, element) => {
  if (arrLikedJokes.length) {
    arr.map(el => {
      const templateJokes = document.querySelector(selectors.templateFavouritesJokes).content.cloneNode(true);
      templateJokes.querySelector(selectors.jokesFavouritesDescription).textContent = el;
      element.append(templateJokes);
    })
  } else {
    printNoJokeToPage();
    buttonDeleteAll.setAttribute('disabled', true);
  }
}

const printNoJokeToPage = () => {
  const noJoke = document.createElement('h2');
  noJoke.classList.add(classes.title);
  noJoke.textContent = 'В избранном пусто';
  containerJokes.append(noJoke);
}

printJokeToPage(arrLikedJokes, containerJokes);

const handlerClickDeleteAllJokes = () => {
  localStorage.clear();
  containerJokes.innerHTML = '';
  printNoJokeToPage();
  buttonDeleteAll.setAttribute('disabled', true);
}

const handlerClickDelete = (e) => {
  const target = e.target;
  if (target.tagName === 'BUTTON' || target.tagName === 'svg' || target.tagName === 'path') {
    const button = target.closest(selectors.buttonDelete);
    let quote = button.nextElementSibling.textContent;
    arrLikedJokes.splice(arrLikedJokes.indexOf(quote), 1);
    containerJokes.innerHTML = '';
    localStorage.setItem("arrLikedJokes", JSON.stringify(arrLikedJokes));
    printJokeToPage(arrLikedJokes, containerJokes);
  }
}

buttonDeleteAll.addEventListener("click", handlerClickDeleteAllJokes);
containerJokes.addEventListener("click", handlerClickDelete);
