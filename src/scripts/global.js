const selectors = {
  navLink: ".js-nav-link",
};

const classes = {
  navLinkActive: 'header__link_active'
}

const navLinkAll = document.querySelectorAll(selectors.navLink);

const arrPathPage = window.location.pathname.split('/');
const pageName = arrPathPage[arrPathPage.length-1];

navLinkAll.forEach(el => {
  if (el.dataset.pageName === pageName) {
    el.classList.add(classes.navLinkActive)
  }
})
